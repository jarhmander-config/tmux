#!/bin/bash
mkdir -p ~/.env
ln -sirT tmux.conf ~/.tmux.conf
ln -sirT tmux ~/.tmux
ln -sir environ ~/.env/tmux.env
